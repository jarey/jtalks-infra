name "jtalks-infra"
version "0.1.0"

depends 'java', '1.28.0'
depends 'ark', '0.9.0'
depends 'apt', '2.6.0'
depends 'git', '4.0.2'
depends 'git_user', '0.3.1'
depends 'ssh-util', '0.6.3'
depends 'mysql', '5.3.6'
depends 'database', '2.2.0'
depends 'nginx', '2.7.4'
depends 'vim', '1.1.2'
depends 'sudo', '2.7.1'
depends 'python', '1.4.6'
depends 'build-essential', '2.0.6'
